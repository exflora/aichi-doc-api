# Overview
This is a simple API which compile [pug](https://github.com/pugjs/pug) template file with given parameters and return the compiled HTML string as response.

# endpoint(s)
## /doc

### method: POST

#### REQUEST
example:

```
{
	"templateName": "nouhin",
	"templateParams": {
		"clientName": "高井勇斗",
		"clientAddress": "東京都千代田区千代田1-1",
		"clientPhone": "09012345678",
		"receiverName": "株式会社exflora",
		"receiverAddress": "東京都品川区東中延2-1-19",
		"receiverPhone": "031234567",
		"arrivalMonth": "12",
		"arrivalDay": "1",
		"arrivalTime": "5:00",
		"product": "観葉植物"
	}
}
```

#### RESPONSE

##### success
sample of response with status code 200

JSON

```
{
  html: <!DOCTYPE html><html lang=\"ja\"><head><meta charset=\"UTF-8\"><title>納品書</title><style type=\"text/css\"></style></head><body>sample</body></html>
}
```

##### error
TBU

# How to develop templates itself locally
Let's say you are trying to develop `example` template.

1. `$ yarn install`
2. Prepare `example` dir with `$ mkdir templates/example`
1. Prepare `template.pug` and `params.js` under the dir `templates/example`
  - `template.pug` is a template file
  - `params.js` defines parameters to be passed to pug compiler later
3. `$ npm_config_pug_template=example yarn run watch_pug`
  - Replace `example` with an actual dirctory name you want to watch and compile
  - It will create index.html and launch a simple hot reload web server hosted on [http://127.0.0.1:8080/](http://127.0.0.1:8080/)

# How to test your template with Zoho creator locally
1. Launch local API Gateway mockup server with `yarn run sls offline`
2. Launch `ngrok` server in order to convert your localhost into a certain external one with `ngrok http 3000`
  - We have to do so because Zoho creator doesn't allow access to localhost from inside of deluge script
3. Develp your Deluge code on Zoho creator to access your template API server
  - e.g. `response = postUrl("http://2636dbe6.ngrok.io/doc",params.toString());`

# How to deploy

## Set up for deployment
1. `$ yarn run setup --overwrite --secret yyyyyyyyy --key xxxxx --profile doc-api-serverless`

## Development env
1. `$ yarn run deploy`

## Production env
1. `$ yarn run deploy -s production`

# Appendix
This application is created based on `aws-nodejs-ecma-script` of [Serverless framework](https://github.com/serverless/serverless) ver1.24.1.