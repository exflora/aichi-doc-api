module.exports = {
  taxPercentage: 0.07,
  items: [
    { productName: 'デザイン', amount: '2', price: '219' },
    {
      productName: '企画・取材費用1', category: '供花', amount: '1', price: '6032',
    },
    {
      productName: '企画・取材費用2', category: '枕花', amount: '1', price: '60000',
    },
    { productName: '企画・取材費用3', amount: '1', price: '60000' },
    { productName: '企画・取材費用4', amount: '1', price: '60000' },
    { productName: '企画・取材費用5', amount: '1', price: '60000' },
    { productName: '企画・取材費用6', amount: '1', price: '60000' },
  ],
  date: '2018/02/01',
  id: 123456789,
  title: '田中家 告別式',
  venue: '林昌寺',
  client: {
    companyName: '株式会社sample',
  },
  source: {
    companyName: '株式会社愛知生花',
    postalCode: '452-0823',
    address: ' 愛知県名古屋市西区あし原町39',
    tel: '052-504-8701',
    fax: '052-504-4187',
  },
};
