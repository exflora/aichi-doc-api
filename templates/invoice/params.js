module.exports = {
  taxPercentage: 0.03,
  client: {
    postalCode: '107-0062',
    address: '東京都港区南青山2丁目11 第一法規本社ビル2階',
    companyName: '株式会社exflora',
    month: '201801',
    date: '2017/12/31',
  },
  source: {
    companyName: '株式会社愛知生花',
    postalCode: '452-0823',
    address: ' 愛知県名古屋市西区あし原町39',
    tel: '052-504-8701',
    fax: '052-504-4187',
    pic: '細川',
    bankName: '中日信用金庫',
    branchName: '西春支店',
    accountType: '普通',
    accountNumber: '0000000',
    accountName: '株式会社　愛知生花',
  },
  main: {
    pastTotalPrice: 150000,
    paidAmount: 150000,
    carriedBalance: '0',
    data: [
      {
        id: '2912-01',
        title: '告別式 １２月１日',
        venue: '浄円寺',
        date: '12/1',
      },

      {
        id: '2912-02',
        title: '告別式 １２月２日 渡邉家',
        venue: 'セレモニーホール工藤',
        date: '12/2',
        items: [
          {
            name: 'らん祭壇',
            amount: 1.0,
            unit: '',
            unitPrice: 500000,
            remark: '',
          },
          {
            name: '棺花',
            amount: 1.0,
            unit: '束',
            unitPrice: 30000,
            remark: '',
          },
        ],
      },

      {
        id: '2912-03',
        title: '告別式 １２月３日',
        venue: '浄円寺',
        date: '12/3',
        items: [
          {
            name: 'らん祭壇',
            amount: 1.0,
            unit: '',
            unitPrice: 500000,
            remark: '',
          },
          {
            name: '棺花',
            amount: 1.0,
            unit: '束',
            unitPrice: 30000,
            remark: '',
          },
          {
            name: '棺花',
            amount: 1.0,
            unit: '束',
            unitPrice: 30000,
            remark: '',
          },
          {
            name: '棺花',
            amount: 1.0,
            unit: '束',
            unitPrice: 30000,
            remark: '',
          },
          {
            name: '棺花',
            amount: 1.0,
            unit: '束',
            unitPrice: 30000,
            remark: '',
          },
          {
            name: '棺花',
            amount: 1.0,
            unit: '束',
            unitPrice: 30000,
            remark: '',
          },
        ],
      },

      {
        id: '2912-04',
        title: '告別式 １２月４日',
        venue: '浄円寺',
        date: '12/4',
        items: [
          {
            name: 'らん祭壇',
            amount: 1.0,
            unit: '',
            unitPrice: 500000,
            remark: '',
          },
          {
            name: '棺花',
            amount: 1.0,
            unit: '束',
            unitPrice: 30000,
            remark: '',
          },
        ],
      },

      {
        id: '2912-05',
        title: '告別式 １２月５日',
        venue: '浄円寺',
        date: '12/5',
        items: [
          {
            name: 'らん祭壇',
            amount: 1.0,
            unit: '',
            unitPrice: 500000,
            remark: '',
          },
          {
            name: '棺花',
            amount: 1.0,
            unit: '束',
            unitPrice: 30000,
            remark: '',
          },
        ],
      },

      {
        id: '2912-06',
        title: '告別式 １２月６日',
        venue: '浄円寺',
        date: '12/6',
        items: [
          {
            name: 'らん祭壇',
            amount: 1.0,
            unit: '',
            unitPrice: 500000,
            remark: '',
          },
          {
            name: '棺花',
            amount: 1.0,
            unit: '束',
            unitPrice: 30000,
            remark: '',
          },
        ],
      },

      {
        id: '2912-07',
        title: '告別式 １２月８日',
        venue: '浄円寺',
        date: '12/8',
        items: [
          {
            name: 'らん祭壇',
            amount: 1.0,
            unit: '',
            unitPrice: 500000,
            remark: '',
          },
          {
            name: '棺花',
            amount: 1.0,
            unit: '束',
            unitPrice: 30000,
            remark: '',
          },
        ],
      },

      {
        id: '2912-08',
        title: '告別式 １２月８日',
        venue: '浄円寺',
        date: '12/8',
        items: [
          {
            name: 'らん祭壇',
            amount: 1.0,
            unit: '',
            unitPrice: 500000,
            remark: '',
          },
          {
            name: '棺花',
            amount: 1.0,
            unit: '束',
            unitPrice: 30000,
            remark: '',
          },
        ],
      },

      {
        id: '2912-09',
        title: '告別式 １２月９日',
        venue: '浄円寺',
        date: '12/9',
        items: [
          {
            name: 'らん祭壇',
            amount: 1.0,
            unit: '',
            unitPrice: 500000,
            remark: '',
          },
          {
            name: '棺花',
            amount: 1.0,
            unit: '束',
            unitPrice: 30000,
            remark: '',
          },
        ],
      },
      {
        id: '2912-10',
        title: '告別式 １２月１０日',
        venue: '浄円寺',
        date: '12/10',
        items: [
          {
            name: 'らん祭壇',
            amount: 1.0,
            unit: '',
            unitPrice: 500000,
            remark: '',
          },
          {
            name: '棺花',
            amount: 1.0,
            unit: '束',
            unitPrice: 30000,
            remark: '',
          },
        ],
      },

      {
        id: '2912-11',
        title: '告別式 １２月１１日 渡邉家',
        venue: 'セレモニーホール工藤',
        date: '12/11',
        items: [
          {
            name: 'らん祭壇',
            amount: 1.0,
            unit: '',
            unitPrice: 500000,
            remark: '',
          },
          {
            name: '棺花',
            amount: 1.0,
            unit: '束',
            unitPrice: 30000,
            remark: '',
          },
        ],
      },
    ],
  },
};
