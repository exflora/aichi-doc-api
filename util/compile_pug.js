const moment = require('moment');
const loadash = require('lodash');

module.exports = (path, _local) => {
  const local = _local;
  local.moment = moment;
  local.loadash = loadash;
  // eslint-disable-next-line
  const template = require(`../templates/${path}`);
  return template(local);
};
